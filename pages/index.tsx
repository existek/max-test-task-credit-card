import type {NextPage} from 'next'
import Head from 'next/head'
import styles from '../styles/Home.module.scss'
import {ActionTypes, CardEnt, useAppState} from './_app'
import Link from 'next/link'
import {renderCardImage} from "./add-card";
import React, {SyntheticEvent, useState} from "react";

const Home: NextPage = () => {
  const { state, dispatch } = useAppState()
  const [filterType, setFilterType] = useState<string>('cardNumber')
  const [filterValue, setFilterValue] = useState<string>('')

  const handleFilterTypeSelect = (e: React.FormEvent<HTMLSelectElement>): void => {
    setFilterValue('')
    setFilterType(e.currentTarget.value)
  }

  const handleFilterInput = (e: React.FormEvent<HTMLInputElement>): void => {
    setFilterValue(e.currentTarget.value)
  }

  const handleDeleteClick = (e: SyntheticEvent, id: string): void => {
    e.stopPropagation()
    e.nativeEvent.stopImmediatePropagation()
    dispatch({
      type: ActionTypes.REMOVE_CARD,
      payload: id
    })
  }

  const cards = state.savedCards.filter(el => el[filterType].includes(filterValue))

  return (
    <div className={styles.container}>
      <Head>
        <title>Your credit cards</title>
        <meta name="description" content="Generated by create next app" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>Your credit cards</h1>
        <div className={styles.filters}>
          <label htmlFor="filterBy">Filter by:</label>
          <select
            name="filterBy"
            id="filterBy"
            onChange={handleFilterTypeSelect}
            defaultValue="cardNumber"
          >
            <option value="cardNumber">Card number</option>
            <option value="cardHolder">Card holder</option>
            <option value="expiration">Card expiration</option>
            <option value="cvv">Card cvv</option>
            <option value="type">Card type</option>
          </select>
          <input
            type="text"
            placeholder="Enter search value"
            onChange={handleFilterInput}
            value={filterValue}
          />
        </div>
        <br/>
        <div>
          {cards.length
            ? (
              cards.map((card: CardEnt) => (
                <div className={styles.userCardItem} key={card.id}>
                  <Link href={`/${card.id}`}>
                    <a>
                      <div className={styles.userCardItemInner}>
                        {renderCardImage(card.cardNumber, 50)}
                        <span className={styles.cardNumber}>
                        {card.cardNumber.replace(card.cardNumber.substr(5, 9), '********')}
                      </span>
                      </div>
                    </a>
                  </Link>
                  <span
                    className={styles.deleteBtn}
                    onClick={(e) => handleDeleteClick(e, card.id)}
                  >
                    X
                  </span>
                </div>
              )))
            : <p>No cards found</p>
          }
        </div>
        <br/>
        <div>
          <Link href="/add-card">
            <a
              className={styles.addCardButton}
            >
              Add new card
            </a>
          </Link>
        </div>
      </main>
    </div>
  )
}

export default Home
