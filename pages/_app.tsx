import '../styles/globals.scss'
import type { AppProps } from 'next/app'
import React, { Component, createContext, useContext, useReducer } from 'react';
import { CardTypes } from './add-card'

export enum ActionTypes {
  ADD_NEW_CARD = 'ADD_NEW_CARD',
  REMOVE_CARD = 'REMOVE_CARD'
}

export type Card = {
  cardNumber: string,
  cardHolder: string,
  expiration: string,
  cvv: string,
  [key: string]: string
}

export type CardEnt = Card & {
  id: string,
  type: string
}

type Action<T> = {
  type: ActionTypes
  payload: T
}

interface AppContextState {
  savedCards: CardEnt[]
}

type Payload = CardEnt | string

interface AppContext {
  state: AppContextState
  dispatch: ({ type }: Action<Payload>) => void
}

interface AppStateProviderProps {
  children: React.ReactNode
}

const initialState = {
  savedCards: [
    {
      id: 'rand_id',
      cardNumber: '5411 2222 3333 4444',
      cardHolder: 'NAME LASTNAME',
      expiration: '11/11',
      cvv: '111',
      type: CardTypes.mastercard
    }
  ]
}

const reducer = (state: AppContextState, action: Action<Payload>): AppContextState => {
  switch (action.type) {
    case ActionTypes.ADD_NEW_CARD: {
      return {
        ...state,
        savedCards: [...state.savedCards, action.payload as CardEnt]
      }
    }
    case ActionTypes.REMOVE_CARD: {
      return {
        ...state,
        savedCards: state.savedCards.filter(card => card.id !== action.payload)
      }
    }
    default: {
      return {
        ...state
      }
    }
  }
}

const AppContext = createContext<AppContext>({} as AppContext)

export const AppStateProvider: React.FC<AppStateProviderProps> =
  ({ children }): JSX.Element => {
    const [state, dispatch] = useReducer(reducer, initialState)
    return (
      <AppContext.Provider value={{ state, dispatch }}>
        {children}
      </AppContext.Provider>
    )
  }

export const useAppState = (): AppContext =>
  useContext(AppContext)

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <AppStateProvider>
      <Component {...pageProps} />
    </AppStateProvider>
  )
}

export default MyApp
